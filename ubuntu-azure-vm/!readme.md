# Create Ubuntu VM in Azure

## Quick setup

Run:

```shell
Run_setting_up_aks_in_a_container\ubuntu-azure-vm\docker-compose.up.bat
```

Login with credentials:

```shell
Login: vscode
Password: Geircode123456
```

## How to create a new VM:
- Rename "ubuntu-vscode-001" to something else i.e. "ubuntu-vscode-002"
- Rename "ubuntuvscode001" to "ubuntuvscode002"
- Create your SSH key by running: \Run_setting_up_aks_in_a_container\dockersecrets\docker-compose.dockersecrets.up.bat
- Insert your SSH Key into "adminPublicKey". Navigate to "Run_setting_up_aks_in_a_container\ubuntu-azure-vm\parameters.json"


