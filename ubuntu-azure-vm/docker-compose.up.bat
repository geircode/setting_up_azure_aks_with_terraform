REM Run Dockerfile.build.bat first if you want to create your own Container Image.
cd %~dp0
docker rm -f setting_up_azure_vm-1
docker rm -f ubuntu-azure-vm_1ae6cd4f_setting_up_azure_vm_run_1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml build
docker-compose -f docker-compose.yml run setting_up_azure_vm
pause
