FROM python:3.6

WORKDIR /app
COPY . /app

RUN apt-get update
RUN apt-get install -y jq

RUN pip install -r requirements.txt 

# Makes it possible to run Docker Commands from inside the Container to the Docker Host
RUN curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-18.06.1-ce.tgz && tar --strip-components=1 -xvzf docker-18.06.1-ce.tgz -C /usr/local/bin

# Get the Terraform files. Get newer version from: https://www.terraform.io/downloads.html
ENV TERRAFORM_VERSION=0.11.8
RUN apt-get -y install openssl unzip wget dos2unix && \
    cd /tmp && \
    wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin && \
    rm -rf /tmp/* && \
    rm -rf /var/cache/apk/* && \
    rm -rf /var/tmp/*

RUN cd /tmp && \
    wget https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/linux/amd64/kubectl && \
    cp kubectl /usr/local/bin && \
    chmod +x /usr/local/bin/kubectl && \
    rm -rf /tmp/*

ENTRYPOINT tail -f /dev/null